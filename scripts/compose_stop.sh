#!/bin/bash
set -ex

PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
COMPOSE_FILE="${PROJECT_ROOT}/docker/docker-compose.yml"

echo
echo "stopping superset-stack"
docker-compose -f "${COMPOSE_FILE}" stop -t 0

echo
echo "if you also want to remove the volumes"
echo "(ie. for a fresh start) run:"
echo
echo "./scripts/compose_purge.sh"
echo
