#!/bin/bash
set -ex

PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
TEMP_DIR="${PROJECT_ROOT}/tmp"

rm "${TEMP_DIR}" -rf
mkdir -p "${TEMP_DIR}"
cd "${TEMP_DIR}"

git clone https://github.com/apache/incubator-superset/
cd incubator-superset/contrib/docker

cp "${PROJECT_ROOT}/build/docker-compose.build.yml" .
cp "${PROJECT_ROOT}/build/superset_config.py" .
echo 'COPY contrib/docker/superset_config.py /home/superset/superset/superset_config.py' >> Dockerfile

docker-compose -f docker-compose.build.yml build --pull superset
docker-compose -f docker-compose.build.yml push superset

rm "${TEMP_DIR}" -rf
