#!/bin/bash
set -ex

PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
COMPOSE_FILE="${PROJECT_ROOT}/docker/docker-compose.yml"

echo 
echo "starting services"
docker-compose -f "${COMPOSE_FILE}" up -d redis postgres

echo
echo "waiting 10 seconds for the database to be ready"
sleep 10

echo
echo "setting up seuperset; also loading samples"
docker-compose -f "${COMPOSE_FILE}" run --rm superset-load-samples

echo
echo "starting superset"
docker-compose -f "${COMPOSE_FILE}" up -d superset

echo
echo "should be available at <server-or-host-ip>:8080"
