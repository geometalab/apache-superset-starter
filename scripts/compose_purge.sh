#!/bin/bash
set -ex

PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
COMPOSE_FILE="${PROJECT_ROOT}/docker/docker-compose.yml"

echo
echo "cleaning/removing superset stack"
docker-compose -f "${COMPOSE_FILE}" down -v --remove-orphans
