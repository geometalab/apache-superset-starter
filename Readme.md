# Apache Superset

## Running

### Using docker

requirements:

* `docker` 
* `docker-compose`

#### starting

```bash
./scripts/compose_start.sh
```

Enter your admin user and password 
(automatically being asked for during setup).


Visit http://localhost:8080

#### stopping

```bash
./scripts/compose_stop.sh
```

#### comeplete removal

```bash
./scripts/compose_purge.sh
```

### Using vagrant and virtualbox

requirements:

* Vagrant
* VirtualBox

```bash
cd vagrant-setup/
vagrant up
```

localhost:8088

Username: admin
Password: superset
